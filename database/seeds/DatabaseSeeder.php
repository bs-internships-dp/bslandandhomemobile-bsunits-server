<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleAndPermissionTableSeeder::class);
        $this->call(DefaultDataSeeder::class);
        $this->call(DefaultCompanySeeder::class);
        $this->call(ContractTemplateV1TableSeeder::class);
    }
}
