<?php

use Illuminate\Database\Seeder;

class DefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        // Warning this seeder should be called on the empty database only
        // Running this seeder on the live production environment could result in the database error

    	// User Identification Default Data
        if ( \App\UserIdentification::count() == 0 ) {
        	\App\UserIdentification::create([ "title" => "Identification Card" ]);
            \App\UserIdentification::create([ "title" => "Passport" ]);
            \App\UserIdentification::create([ "title" => "Student Card" ]);
        }

        // Default Roles and Permissions
        // $this->call(RoleAndPermissionTableSeeder::class);

        // Default Built-in User
        $user = \App\User::where('phone_number', '0123456789')->first();
        if ( !$user ) {
            $user = \App\User::create([         
                "name"          => "SYSTEM",
                "email"         => "",
                "phone_number" => "0123456789",
                "password"      => bcryt("Admin@123")
            ]);
            $user->assignRole( 'administrator' );
        }
    }
}