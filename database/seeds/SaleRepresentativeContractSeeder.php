<?php

use App\Contract;
use Illuminate\Database\Seeder;

class SaleRepresentativeContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$contracts = Contract::with(['unit', 'unit.unitType', 'unit.unitType.project'])->get();
    	foreach( $contracts as $contract ) {
    		$contract->sale_representative_id = $contract->unit->unitType->project->sale_representative_id;
    		$contract->save();
    	}
    }
}
