<?php

namespace App\Http\Controllers\Api;

use App\Unit;
use App\UnitAction;
use Illuminate\Http\Request;
use App\Http\Resources\Unit as UnitResource;
use App\Http\Resources\UnitCollection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
   	public function all(Request $request) 
   	{	
   		$units = Unit::query();
   		if ( $request->input("embed") ) {			
			$relationships = explode(',', trim($request->input('embed')) );
			$units = $units->with($relationships);
		}

		if ( $request->input("term") ) {		
			$units = $units->where('code' , 'LIKE', '%'. $request->input("term").'%'); 
		}

		if ( $request->input('unit_type_id') ) {
			$units = $units->where('unit_type_id', $request->input('unit_type_id'));
		} elseif( $request->input('project_id') ) {
			$unit_type_in_array = \App\UnitType::where('project_id', $request->input('project_id'))->get()->pluck('id');
			$units = $units->whereIn('unit_type_id', $unit_type_in_array);
		}

		if ( $request->input('status') ) {
			$actions = explode(',', trim($request->input('status')));
			$units = $units->whereHas('action', function ($query) use ($actions) {
                $query->whereIn('action', $actions );            
            });
		}

 		return new UnitCollection( $units->saleable()
 									     ->publishedProject()
 								         ->paginate($request->input('per_page')) );
   	}

   	public function show(Request $request, $id)
   	{   		
   		$unit = Unit::findOrFail($id);
   		if ( $request->input("embed") ) {			
			$relationships = explode(',', trim($request->input('embed')) );
			$unit->load($relationships);
		}

		return new UnitResource($unit);
   	}

   	public function getAvailabilityStatistic(Request $request) 
   	{
   		$unit_table_name = with(new Unit)->getTable();
   		$unit_action_table_name = with(new UnitAction)->getTable();
  		try {
			$query = DB::table($unit_table_name)
					   ->join($unit_action_table_name, $unit_table_name.'.unit_action_id', '=', $unit_action_table_name.'.id' )
					   ->groupBy($unit_action_table_name.'.action')
					   ->select(DB::raw('count(units.id) as count'), 'unit_actions.action');

			return response()->json([ 
	            'data' => $query->get(),
	            'code' => 200,
	            'message' => __('Success')
	        ]);
  		} catch (\Exception $e) {
  			return $this->sendErrorJsonResponse( __('There are some problems while trying to updating your request'), 500);
  		}
   	}
}
