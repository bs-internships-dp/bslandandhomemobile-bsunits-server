<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Project;
use App\UnitType;
use App\ContractTemplate;
use App\SaleRepresentative;
use App\Bank;
use App\Http\Requests\ProjectRequest;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view-project')->only('index');
        $this->middleware('permission:create-project')->only(['create','store']);
        $this->middleware('permission:update-project')->only(['edit','update']);
        $this->middleware('permission:delete-project')->only(['showDeleteForm','showRetoreForm','destroy','restore']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $projects = Project::query();
        $projects = $projects->withCount(['unitTypes']);

        if ( $request->query('status') ) {
            $projects = $projects->ofStatus($request->query('status'));
        } else {
            $projects = $projects->ofStatus();
        }

        if ( $request->query('term') ) {
            $term = $request->query('term');
            $projects = $projects->where('name' , 'LIKE', '%'.$term.'%');
            $projects = $projects->orWhere('name_en' , 'LIKE', '%'.$term.'%');
            $projects = $projects->orWhere('short_code' , 'LIKE', '%'.$term.'%');            
        }

        $projects = $projects->paginate();

        return view('admin.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        $sale_representatives = SaleRepresentative::all();
        $banks =  Bank::all();
        return view('admin.project.new', compact("sale_representatives","banks",'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {      
        $data = $request->validated();
        $data['user_id'] = Auth::id();

        try {
            if ( $request->hasFile('logo_url') ) {                                        
                $path = $request->file('logo_url')->store("project_logo","public");
                Image::make('storage/'.$path)->resize(200, 200)->save('storage/'.$path); 
                $data['logo_url'] = $path;
            }

            if ( $request->hasFile('master_plan_url') ) {                                        
                $path = $request->file('master_plan_url')->storeAs("master_plans",md5(time()).'.svg',"public");              
                $data['master_plan_url'] = $path;
            }

            $project = Project::create($data);
            return redirect()->route('admin.projects.edit', ['id' => $project->id])
                             ->with('status', "Project has been created successfully.");
        } catch (\Exception $e) {
            return back()->withInput()->withErrors([ 'project' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $companies = Company::all();
        $project = Project::findOrFail($id);
        $unit_types = UnitType::where('project_id',$id)->withCount(['paymentOptions','contractTemplate'])->get();
        $contract_templates = ContractTemplate::get(['id','name']);
        $sale_representatives = SaleRepresentative::all();
        $banks =  Bank::all();
        return view('admin.project.edit',compact("project","unit_types","contract_templates","sale_representatives","banks",'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $project = Project::findOrFail($id);      
        $data = $request->validated();
        $data['user_id'] = Auth::id();     
        try {           
            if ( $request->hasFile('logo_url') ) {
                $project->deleteLogoUrl();
                $path = $request->file('logo_url')->store("project_logo","public");                
                Image::make('storage/'.$path)->resize(200, 200)->save('storage/'.$path);
                $data['logo_url'] = $path;
            }

            if ( $request->hasFile('master_plan_url') ) {
                $project->deleteMasterPlanUrl();
                $path = $request->file('master_plan_url')->storeAs("master_plans",md5(time()).'.svg',"public");
                $data['master_plan_url'] = $path;
            }

            $project = $project->fill($data);
            $project->save();
            return redirect()->route('admin.projects.edit', ['id' => $project->id])
                             ->with('status', "Project has been updated successfully.");

        } catch (\Exception $e) {
            return back()->withInput()->withErrors([ 'project' => $e->getMessage()]);
        }
    }

    public function showDeleteForm($id) 
    {
        $project = Project::findOrFail($id);
        return view('admin.project.delete',compact("project"));
    }

    public function showRestoreForm($id) 
    {
        $project = Project::withTrashed()->findOrFail($id);
        return view('admin.project.restore',compact("project"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        try {       
            $project->delete();
            return redirect()->route('admin.projects.index')->with('status', "Project : $project->name has been successfully deleted.");
        } catch (\Exception $e) {
            return back()->withErrors([ 'project' => $e->getMessage()]);
        }       
    }

    public function restore($id) 
    {
        $project = Project::withTrashed()->findOrFail($id);
        try {         
            $project->restore();
            return redirect()->route('admin.projects.index')->with('status', "Project : $project->name has been successfully restored.");
        } catch (\Exception $e) {
            return back()->withErrors([ 'project' => $e->getMessage()]);
        }
    }

    public function deleteMedia(Request $request, $id) {
        $project = Project::withTrashed()->findOrFail($id);

        try {
            $media_type  = $request->media_type ?? "master_plan_url";
            switch ($media_type) {
                case 'master_plan_url':
                    $project->master_plan_url  = null;
                    $project->save();
                    $project->deleteMasterPlanUrl();
                    break;                
                default:
                    return response()->json([ 'message' => __('Incorrect media type!') ], 422);
            }
            return response()->json([ 'message' => __('Media has been removed successfully.') ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([ 'message' => __('Internal Server Errors!') ], 500);
        }
    }
}
