<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Helpers\UserRole;
use App\Traits\UserActiveStatusNotification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
	use UserActiveStatusNotification;

	protected $validationRules =  [
		'name' 				=> 'required|string|min:6|max:191', 
		'email' 			=> "nullable|email|unique:users,email",				        
		'phone_number'  	=> "required|regex:^0\d{8,9}^|unique:users,phone_number",
		'gender' 			=>  'in:Male,Female',
		'birthdate' 	    => 'date',
		'managed_by'        => "required|integer|min:0",
		'avatar'			=> 'image|dimensions:min_width=256,min_height=256,max_width=1028,max_height=1028',
		'identification_id' => 'exists:user_identifications,id'
	];

	public function index(Request $request)   
	{
		$roles = Role::get(['id','name']);
		$users = User::query();
		$users = $users->with(['roles']);

		if ( $request->query('term') ) {
			$term = $request->query('term');
			$users = $users->where('name' , 'LIKE', '%'.$term.'%');
			$users = $users->orWhere('email' , 'LIKE', '%'.$term.'%');
			$users = $users->orWhere('phone_number' , 'LIKE', '%'.$term.'%');
		}

		if ( $request->query("role") AND $request->query("role") != "" ) {
			$users = $users->role($request->query("role"));
		}

		if ( $request->query('verified') ) {
			$is_verified = strcasecmp($request->query('verified'), 'true') == 0 ? true : false;
			$users = $users->ofVerified($is_verified);
		}

		if ( $request->query('active') ) {
			$is_active = strcasecmp($request->query('active'), 'true') == 0 ? true : false;
			$users = $users->ofActive($is_active);
		}

		if ( auth()->user()->hasRole(UserRole::SALE_MANAGER) ) {
			$users = $users->role([UserRole::SALE_TEAM_LEADER, UserRole::AGENT]);
		}

		$users = $users->paginate(10);

		return view('admin.user.index', compact('users','roles'));
	}

	public function edit(Request $request, $id)
	{
		$user = User::with(['roles'])->findOrFail($id);
		$roles = Role::all();
		$users = User::all();	
		return view('admin.user.edit', compact('user','roles','users'));
		
	}

	public function create()
	{
		$roles = Role::all();
		$users = User::all();
		return view('admin.user.new',compact('roles', 'users'));
	}

	public function show(Request $request, $id) 
	{
		// It is used on Contract Page.
		if ($request->ajax()) {
			return $this->getUserAjaxResposne($id);
		}

		$user = User::query();		
		$auth_role = auth()->user()->roles()->first();
	
		switch ($auth_role->name) {
			case UserRole::ADMINISTRATOR:
				$user = User::findOrFail($id);				
				break;
			case UserRole::SALE_MANAGER:
				$user = User::role([UserRole::AGENT, UserRole::SALE_TEAM_LEADER])
						->where('id', $id)
						->firstOrFail();
				break;
			default:
				return abort(404);
				break;
		}

		return view('admin.user.single', compact('user'));
	}

	public function store(Request $request)
	{
		$validation_rule = $this->getValidationRules();
		$validation_rule['password'] = "required|string|min:6|confirmed";

		$validatedData = $request->validate($validation_rule);

		try {			
			$user = User::create([
				'name' => $request->name,
				'email' => $request->email,
				'phone_number' => $request->phone_number,
				'gender' => $request->gender,
				'need_change_password' => $request->need_change_password ? 1 : 0,
				'active' => $request->active ? 1 : 0,
				'verified' => $request->verified ? 1 : 0,
				'password' => bcrypt($request->password),
				'managed_by' => $request->managed_by
			]);

			$role = Role::findOrFail($request->role);

			$user->syncRoles($role);

			return redirect()->route('admin.user.edit', ['id' => $user->id])
						 	 ->with('status', "User has been created successfully.");
		} catch (\Exception $e) {
			return back()->withInput()->withErrors([ 'user' => $e->getMessage()]);
		}
	}

	public function update(Request $request, $id)
	{
		$user = User::findOrFail($id);
		$role = Role::findOrFail($request->role);
		$phone_number_changed = false;

		$validation_rule = $this->getValidationRules();
		$validation_rule['phone_number'] = "required|regex:/0[0-9]{8,9}/|between:9,10|unique:users,phone_number,".$user->id;
		$validation_rule['email'] = "nullable|email|unique:users,email,".$user->id;

		if ( strcmp( $request->phone_number, $user->phone_number ) != 0 ) {
			$phone_number_changed = true;
		}

		$validatedData = $request->validate($validation_rule);

		try {
			$user->fill($request->all());			
			$user->active = $request->active ? 1 : 0;
			$user->verified = $request->verified ? 1 : 0;

			if ( $user->isDirty('active') ) {			
				$this->sendUserActiveStatusNotification($user);
			}

			$user->save();
			if ( $phone_number_changed ) {
				$user->tokens()->delete();
			}
			if ( !$user->hasRole($role) ){
				$user->tokens()->delete();
				$user->syncRoles($role);
			}
		} catch (\Exception $e) {
			return back()->withInput()->withErrors(['user' => $e->getMessage()]);
		}
		return back()->with('status', 'User has been updated successfully');
	}

	public function showPasswordForm($id){
		$user = User::findOrFail($id);
		return view('admin.user.password_reset', compact('user'));
	}

	public function resetPassword(Request $request, $id)
	{
		$user = User::findOrFail($id);

		$validatedData = $request->validate([
	        'password' => 'required|string|min:6|confirmed'
	    ]);

	    try {
	    	$user->password = bcrypt($request->password);	    
	    	$user->need_change_password = $request->input('need_change_password') ? 1 : 0;
	    	$user->save();
	    	$user->tokens()->delete();
	    	
	    } catch (\Exception $e) {
	    	return back()->withErrors([ 'password' => $e->getMessage()]);
	    }

		return back()->with('status', "Password has been reset successfully.");
	}

	public function getValidationRules(){
		return $this->validationRules;
	}

	public function getAgents(Request $request) 
	{		
		$agents = User::role([UserRole::AGENT,UserRole::SALE_TEAM_LEADER])->with(['manager']);
		if ( $request->ajax() ) {
			if ( $request->query('term') ) {
	            $term = $request->query('term');
	            $agents = $agents->where('name' , 'LIKE', '%'.$term.'%');      
	        }		
			return response()->json( $agents->simplePaginate() );
		}
		return $agents->get();
	}

	private function getUserAjaxResposne($id) {
		return response()
            ->json(User::findOrFail($id));
	}
}
