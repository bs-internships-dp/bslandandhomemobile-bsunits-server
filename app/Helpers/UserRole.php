<?php
namespace App\Helpers;
 
class UserRole {
    const __default = self::AGENT;
    
    const ADMINISTRATOR = "administrator";
    const SALE_MANAGER = "sale_manager";
    const UNIT_CONTROLLER = "unit_controller";
    const SALE_TEAM_LEADER = "sale_team_leader";
    const AGENT = "agent";
    const CONTRACT_CONTROLLER = "contract_controller";
    const ACCOUNTANT = "accountant";
    const REPORT = 'report';
}