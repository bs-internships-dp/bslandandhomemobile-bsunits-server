<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', '/login', 301);
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', "Auth\LoginController@authenticate");
Route::post('/logout', "Auth\LoginController@logout")->middleware(['auth'])->name('logout');
Route::get("/change-password", "Auth\ChangePasswordController@showChangePasswordForm")->middleware(['auth'])->name('password.change');
Route::post("/change-password", "Auth\ChangePasswordController@changePassword")->middleware(['auth']);

// Contract Template Route
Route::get("/contract_template/{template_path}/preview", "ContractTemplatePreviewController@preview")
     ->name('admin.contract_templates.preview');
Route::get("/ios/install", "AppDownloadController@showIOSDownloadView")->name("app_download.ios");
Route::get("/ios/install/dev", "AppDownloadController@showIOSDevDownloadView")->name("app_download.ios_dev");
Route::get('/contract/templates', 'ContractTemplatePreviewController@showTemplate')
    	 ->name('contracts.template')
    	 ->middleware(['role:contract_controller']);
// End Contract Template Route

Route::namespace("Admin")->prefix('admin')->name("admin.")->middleware(['auth'])->group( function () {
	Route::get('/users', "UserController@index")->name('user.all')
		 ->middleware('permission:view-user');
	Route::get('/users/new', "UserController@create")->name('user.new')
	     ->middleware('permission:create-user');
	Route::get('/users/get_agents', "UserController@getAgents")->name('user.role.agent');
	Route::get('/users/{id}', 'UserController@show')->name('user.show');
	Route::get('/users/{id}/tokens', "UserTokenController@show")->name('user.token.show')
		 ->middleware('permission:view-user');
	Route::get('/users/{id}/edit', "UserController@edit")->name('user.edit')
		 ->middleware('permission:update-user');
	Route::get('/users/{id}/password/reset', "UserController@showPasswordForm")->name('user.password.show')
		 ->middleware('permission:update-user');
	Route::post('/users/{id}/password/reset', "UserController@resetPassword")->name('user.password.reset')
		 ->middleware('permission:update-user');
	Route::post('/users', "UserController@store")->name('user.create')
		 ->middleware('permission:create-user');
	Route::put('/users/{id}', "UserController@update")->name('user.update')
		 ->middleware('permission:update-user');

	Route::delete('/tokens/{id}', "UserTokenController@destroy")->name('user.token.delete')
		 ->middleware('permission:update-user');

	Route::middleware('role:administrator')->group( function () {
		Route::get('/roles', "RoleController@index")->name('role.all');
		Route::get('/roles/new', "RoleController@create")->name('role.new');
		Route::get('/roles/{id}/edit', "RoleController@edit")->name('role.edit');
		Route::get('/roles/{id}/remove', "RoleController@remove")->name('role.remove');	
		Route::post('/roles', "RoleController@store")->name('role.create');
		Route::put('/roles/{id}', "RoleController@update")->name('role.update');
		Route::delete('/roles/{id}', "RoleController@delete")->name('role.delete');
	});

	Route::get('/contract_requests', "ContractRequestController@index")
		 ->name('contract_request.all');	
	Route::get('/contract_requests/{id}', "ContractRequestController@show")
		 ->name('contract_request.show');
	Route::put('/contract_requests/{id}', "ContractRequestController@update")
	     ->name('contract_request.update');   
	Route::get('/contract_requests/{id}/view_payment_sample', 'ContractRequestController@showMortgage')
		 ->name('contract_request.view_payment_sample');

	// Unit Contract Request
	Route::get("/unit_contract_requests", "UnitContractRequestController@index")
	     ->name('unit_contract_requests.index');	
	Route::get("/unit_contract_requests/{id}", "UnitContractRequestController@show")
	     ->name('unit_contract_requests.show');
	Route::get("/unit_contract_requests/{id}/createContract", "UnitContractRequestController@showCreateContractForm")
	     ->name('unit_contract_requests.create_contract')
	      ->middleware('permission:create-contract');;
	Route::get('/unit_contract_requests/{id}/print', "UnitContractRequestController@printRequest")
		 ->name('unit_contract_requests.print');

	// Unit Deposit Request
	Route::get("/unit_deposit_requests", "UnitDepositRequestController@index")
	     ->name('unit_deposit_requests.index')
	     ->middleware('permission:view-unit-deposit-request');
	Route::get("/unit_deposit_requests/{id}", "UnitDepositRequestController@show")
	     ->name('unit_deposit_requests.show')
	     ->middleware('permission:view-unit-deposit-request');
	Route::get("/unit_deposit_requests/{id}/updateReceivingAmount", "UnitDepositRequestController@showUpdateReceivingAmountForm")
	     ->name('unit_deposit_requests.update.receving_amount')
	     ->middleware('permission:view-unit-deposit-request');
	Route::post("/unit_deposit_requests/{id}/updateReceivingAmount", "UnitDepositRequestController@UpdateReceivingAmount")
		 ->middleware('permission:update-unit-deposit-request');
	Route::post('/unit_deposit_requests/{id}/cancel', 'UnitDepositRequestController@cancel')
		 ->name('unit_deposit_requests.cancel')
		 ->middleware('permission:cancel-unit-deposit-request');
	Route::post('/unit_deposit_requests/{id}/void', 'UnitDepositRequestController@void')
		 ->name('unit_deposit_requests.void')
		 ->middleware('permission:void-unit-deposit-request');
	Route::get("/unit_deposit_requests/{id}/getPayment", "UnitDepositRequestController@getPayment")->name('unit_deposit_requests.payment');

	// Unit Hold Request
	Route::get('/unit_hold_requests', 'UnitHoldRequestController@index')
		 ->name('unit_hold_requests.index')
	     ->middleware('permission:view-unit-hold-request');
	Route::get("/unit_hold_requests/{id}", "UnitHoldRequestController@show")
	     ->name('unit_hold_requests.show')
	     ->middleware('permission:view-unit-hold-request');

	// Contract Route
	Route::get('/contracts', "ContractController@index")
		 ->name('contracts.index')
		 ->middleware('permission:view-contract');

	Route::get('/contracts/export', 'ContractController@export')
		 ->name('contracts.export')
		 ->middleware('permission:view-contract');

	Route::get('/contracts/create', "CreateContractController@showCreateForm")
		 ->name('contracts.create')
		 ->middleware('permission:create-contract');

	Route::get('/contracts/{id}/edit', "EditContractController@showEditForm")
		 ->name('contracts.edit')
		 ->middleware('permission:update-contract');

    Route::get('/contracts/{id}/print', 'ContractPdfController@getContractPdfFile')
		 ->name('contracts.print')
		 ->middleware('permission:view-contract');

    Route::get('/contracts/{id}/upload', 'UploadContractController@showContractUploadForm')
		 ->name('contracts.upload')
		 ->middleware('permission:update-contract');
    Route::post('/contracts/{id}/upload', 'UploadContractController@uploadContractPDF')
    	 ->middleware('permission:update-contract');;

	Route::post('/contracts/create', "CreateContractController@store")
	     ->middleware('permission:create-contract');

	Route::post('/contracts/{id}/cancel', "CancelContractController@processCancellationRequest")
	     ->middleware('permission:cancel-contract');
	Route::post('/contracts/{id}/void', "VoidContractController@processVoidRequest")
	     ->middleware('permission:void-contract');

	Route::put('/contracts/{id}', "EditContractController@update")
		 ->name('contracts.update')
		 ->middleware('permission:update-contract');
	// End Contract Route

	// Contract Attachment Route
    Route::delete('/contract_attachments/{id}', "ContractAttachmentController@delete")
         ->name('contract_attachments.destroy');
	// Contract Attachment Route

	// Loan Route
	Route::post('/loan/amortization-schedule', "LoanController@getAmortizationSchedule")
		 ->name('loan.amortization-schedule');
    Route::get('/loan/payment_date', "LoanController@paymentDate");
	// End Loan

	// Banner
    Route::resource('banners', 'BannerController');
	// End Banner

	// Bank
    Route::resource('banks', 'BankController');
	// End Bank

	// Banner
    Route::resource('categories', 'CategoryController');   
	// End Banner

   	// Banner
    Route::resource('posts', 'PostController');    
	// End Banner

	Route::resource('projects', 'ProjectController');
	Route::get('projects/{id}/delete', "ProjectController@showDeleteForm")->name('projects.delete');
	Route::get('projects/{id}/restore', "ProjectController@showRestoreForm")->name('projects.restore');
	Route::post('projects/{id}/restore', "ProjectController@restore");
	Route::delete('projects/{id}/media', 'ProjectController@deleteMedia')->middleware('permission:update-project');

	Route::resource('companies', 'CompanyController');
	Route::get('companies/{id}/delete', 'CompanyController@delete')->name('companies.delete');

	Route::resource('payment_options', 'PaymentOptionController');
	Route::get('payment_options/{id}/delete', "PaymentOptionController@showDeleteForm")->name('payment_options.delete');
	Route::get('payment_options/{id}/restore', "PaymentOptionController@showRestoreForm")->name('payment_options.restore');
	Route::post('payment_options/{id}/restore', "PaymentOptionController@restore");
	Route::get('payment_options/{id}', "PaymentOptionController@show");

	// Discount Promtion Route
	Route::resource('discount_promotions', 'DiscountPromotionController');
	Route::post('discount_promotions/{id}/discount_promotion_items', 'DiscountPromotionController@addItem');
	Route::delete('discount_promotions/{id}/discount_promotion_items/{item_id}', 'DiscountPromotionController@removeItem');

	Route::resource('unit_types', 'UnitTypeController');
	Route::get('unit_types/{id}/delete', "UnitTypeController@showDeleteForm")->name('unit_types.delete');
	Route::get('unit_types/{id}/restore', "UnitTypeController@showRestoreForm")->name('unit_types.restore');
	Route::post('unit_types/{id}/restore', "UnitTypeController@restore");	
	Route::get('unit_types/{id}/saleable', "UnitTypeController@showSetSaleableStatusForm")->name('unit_types.set_saleable_status');
	Route::post('unit_types/{id}/saleable', "UnitTypeController@SetSaleableStatus");	
	Route::get('unit_types/{id}/payment_option', "UnitTypeController@getPaymentOption");
	Route::get('unit_types/{id}/clone_payment_option',"UnitTypeController@getClonePaymentOptionForm")->name("unit_types.clone");
	Route::post('unit_types/{id}/clone_payment_option',"UnitTypeController@clone");
	Route::post('unit_types/{id}/addMedia', 'UnitTypeController@addMedia')->name('unit_types.add_media');
	Route::delete('unit_types/{id}/deleteMedia', 'UnitTypeController@deleteMedia')->name('unit_types.delete_media');

	// Unit Route
	Route::get('/units', "UnitController@index")
		 ->name("units.index")->middleware('permission:view-unit');
	Route::get('/units/new', "UnitController@create")
		 ->name("units.create")->middleware('permission:create-unit');
	Route::get('/units/import', "UnitController@showImportForm")
		 ->name("units.import")->middleware('permission:import-unit');
    Route::post('/units/import', "UnitController@import")->middleware('permission:import-unit');
    Route::get('/units/bulkStatusModify', 'UnitStatusModifyController@showForm')
	     ->name('units.bulk_status_modify')->middleware('permission:modify-status-unit');
	Route::post('/units/bulkStatusModify', 'UnitStatusModifyController@modifyStatus')
		 ->middleware('permission:modify-status-unit');
    Route::get('/units/import_template', "UnitController@getImportTempalte")
    	 ->name("units.import_template")->middleware('permission:import-unit');
	Route::get('/units/export', "UnitController@showExportForm")
	 	 ->name("units.export")->middleware('permission:export-unit');
	Route::get('/units/saleReport', "UnitController@exportSaleReport")
	 	 ->name("units.report.sale")->middleware('permission:export-unit');

	Route::post('/units/export', "UnitController@export")->middleware('permission:export-unit');
    Route::get('/units/{id}/edit', "UnitController@edit")    
		 ->name("units.edit")->middleware('permission:update-unit');		
	Route::get('/units/{id}', "UnitController@show")
		 ->name("units.show")->middleware('permission:view-unit');
	Route::get('/units/{id}/actions', "UnitController@getActions")
		 ->name("units.get.actions")->middleware('permission:view-unit');
	Route::get('/units/{id}/activities', "UnitController@getActivities")
		 ->name("units.get.activities")->middleware('permission:view-unit');
	Route::get('/units/{id}/change_status', "UnitController@showChangeStatusForm")
		 ->name("units.status.change");
	Route::post('/units/{id}/change_status', "UnitController@changeStatus");	
    Route::post('/units', "UnitController@store")
         ->name('units.store')->middleware('permission:create-unit');
    Route::put('/units/{id}', "UnitController@update")
         ->name('units.update')
         ->middleware('permission:update-unit'); 
    Route::post('/units/{id}/updateSaleable', "UnitController@updateSaleableStatus")
         ->name("units.saleable")->middleware('permission:update-unit');
    Route::post('/units/{id}/updateActive', "UnitController@updateActiveStatus")
    	 ->name("units.saleable")->middleware('permission:update-unit');
	// End Unit Route
	
	// User member
	Route::get('/user/{id}/members', "UserController@getMembers")
		 ->name("user.get.members");
	// End User member

	// Unit Action Route
    Route::get('/unit_actions', "UnitActionController@index")
    	 ->name('unit_actions.index');
	// End Unit Action Route

   	// Sale Representative
   	Route::resource('sale_representatives', 'SaleRepresentativeController');
   	Route::get('sale_representatives/{id}/delete', "SaleRepresentativeController@showDeleteForm")->name('sale_representatives.delete');
    // End Sale Representative

   	// Mobile App Version
   	Route::resource('app_versions', 'AppVersionController');
   	// End Mobile App Version

   	// Miscellanous Route          	
	Route::get('/notifications', "NotificationController@showNotification")
		 ->name('notification.show');
	Route::get('/notifications/readAll', "NotificationController@markAllAsRead")
		 ->name('notification.mark.read.all');
	Route::post('/notifications/{id}/markAsRead', "NotificationController@markAsRead")
		 ->name('notification.mark.read');
	// End Miscellanous route
});
