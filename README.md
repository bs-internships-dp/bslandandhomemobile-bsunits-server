## Installation Note
Clone the repository

    `git clone https://gitlab.com/hiayongkuy/bslandhome.git`

Install PHP Dependency 

    `composer install`

Copy .env.example to .env and edit it to match your application setting

    `mv .env.example .env`

Generate application key

    `php artisan key:generate`

Run database migration 

    `php artisan migrate`

Insert Default Data to database

    `php artisan db:seed --class=RoleAndPermissionTableSeeder`
    `php artisan db:seed --classDefaultDataSeeder`
Create a Symlink directory for file upload

    `php artisan storage:link`

The application use [barryvdh/laravel-snappy](https://github.com/barryvdh/laravel-snappy) and [h4cc/wkhtmltopdf-amd64](https://github.com/h4cc/wkhtmltopdf-amd64) to workaround with PDF. Please refer to library homepage for installation and configuration.

## Developer Note
### DateTime object Format
This application provide the configuration of DateTime Format in __app/config/app.php__
2 elements were added to this configuration file (.env key : PHP_DATE_FORMAT, PHP_DATETIME_FORMAT)

1. __php_date_format__ :  the format which you want to echo by Carbon function  toSystemDateString()
2. __php_datetime_format__ : the format which you want to echo by Carbon function  toSystemDateTimeString()

the two funcitons were added in __app/Providers/AppServiceProvider.php. So, whenever you want to display the format from the above 2 variables please use these 2 function on Carbon Object accordingly.
About Carbon DateTime please refer to : [https://carbon.nesbot.com/docs/](https://carbon.nesbot.com/docs/)

for UI, this application is used [bootstrap-datepicker](https://bootstrap-datepicker.readthedocs.io/en/latest/). One more key (__js_date_format__) was added to  __app/config/app.php__ for managing the Date format of this JS library. 

In order to format back to Database friendly Date object, one funciton was added to __app\Http\Controllers\Controller.php__
    
    public function covertToStandardDateFormat($data, $date_key_array)
     {        
        foreach( $date_key_array as $key ) {
            if ( ! isset($data[$key]) ) {
                continue;
            }
            $format = config('app.php_date_format');
            $data[$key] = \Carbon\Carbon::createFromFormat($format, $data[$key])->startOfDay()->format('Y-m-d');
        }
        return $data;
    }
    
the function take 2 arguments 
    -   $data : array (key => value) of user input, (e.g. $request->input())
    -   $date_key_array : array of keys which you want to convert the custom format to Database friendly format.

Suppose, you set the PHP_DATE_FORMATE="d-m-Y" and JS_DATE_FORMAT="dd-mm-yyyy" in your .env file, your .datepicker input will return "23-01-2019". So, you use this function to transform it back to "2019-01-23"

Example of store and update function in controller : 

    public function store(Request $request)
    {        
        $your_model = New YourModel();
        $data = $this->covertToStandardDateFormat($request->input(), $your_model->getDates());
        
        $your_model->fill($data);
        $your_model->save()
        
    }
    
    public function update(Request $request, $id)
    {
        $your_model = YourModel::findOrFail($id);
        $data = $this->covertToStandardDateFormat($request->input(), $your_model->getDates());
        
        $your_model->fill($data);
        $your_model->save()
    }



## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
