<?php

return [
    'available_permissions' => [        
        /*
         * In Seeder file, when you run php artisan db:seed command, it will create
         * Permission model according to this setting
         *        
         */
        'dashboard' => [ 'view' ],
        'user' => [ 'view', 'create', 'update', 'delete' ],
        'contract' => [ 'view', 'create', 'update', 'delete', 'cancel', 'void' ],
        'project' => [ 'view', 'create', 'update', 'delete' ],
        'unit-type' => [ 'view', 'create', 'update', 'delete' ],
        'unit' => [ 'view', 'create', 'update', 'delete', 'import', 'export', 'modify-status'],
        'unit-handover'=> [ 'view', 'import', 'export' ],
        'discount-promotion' => [ 'view', 'create', 'update', 'delete' ],
        'payment-option' =>  [ 'view', 'create', 'update', 'delete' ],
        'sale-representative' =>  [ 'view', 'create', 'update', 'delete' ],
        'unit-hold-request' => [ 'view', 'create', 'update', 'delete', 'approve', 'reject' , 'cancel'  ],
        'unit-deposit-request' => [ 'view', 'create', 'update', 'delete', 'approve', 'reject' , 'cancel', 'void' ],
        'unit-contract-request' => [ 'view', 'create', 'update', 'delete', 'approve', 'reject' , 'cancel', 'void' ],
        'category' => [ 'view', 'create', 'update', 'delete'],
        'post' => [ 'view', 'create', 'update', 'delete'],
        'project-statistics' => [ 'view' ]
    ],

    'available_roles' => [
        /*
         * In Seeder file, when you run php artisan db:seed command, it will create
         * Role model according to this setting
         *        
         */
        'administrator' => [ 
            'dashboard', 
            'user',
            'contract',
            'project',
            'unit-type',
            'unit',
            'unit-handover',
            'discount-promotion', 
            'sale-representative',
            'payment-option',
            'category',
            'post',
            'unit-hold-request' => ['view'],
            'unit-deposit-request' => ['view'],
            'unit-contract-request' => ['view'],
        ],
        'sale_manager' => [ 
            'dashboard', 
            'unit-deposit-request' => ['view', 'approve', 'reject'],
            'unit-contract-request' => ['view'],
            'unit' => ['view'],
        ],
        'unit_controller' => [ 
            'dashboard', 
            'unit-deposit-request' => ['view', 'approve', 'reject'],
            'unit-contract-request' => ['view'],
            'unit',
            'unit-handover'
        ],
        'sale_team_leader'=> [             
            'unit-hold-request' => [ 'view', 'create', 'update', 'cancel' ],
            'unit-deposit-request' => [ 'view', 'create', 'update' ],
            'unit-contract-request' => [ 'view', 'create', 'update', 'cancel' ],
            'unit' => [ 'view' ]
        ],
        'contract_controller' => [ 
            'dashboard',
            'contract',
            'unit-deposit-request' => ['view'],
            'unit-contract-request' => ['view'],
            'unit' => ['view']
        ],
        'accountant' => [ 
            'dashboard', 
            'unit-deposit-request' => ['view', 'void' , 'cancel'],
            'unit' => [ 'view' ]
        ],
        'agent' => [             
            'unit-hold-request' => [ 'view', 'create', 'update', 'cancel' ],
            'unit-deposit-request' => [ 'view', 'create', 'update' ],
            'unit-contract-request' => [ 'view', 'create', 'update', 'cancel' ],
            'unit' => [ 'view' ],
        ],
        'editor' => ['dashboard', 'category', 'post'],
        'report' => [ 'project-statistics' ]
    ],

    'default_role' => "agent",

    'models' => [
        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your permissions. Of course, it
         * is often just the "Permission" model but you may use whatever you like.
         *
         * The model you want to use as a Permission model needs to implement the
         * `Spatie\Permission\Contracts\Permission` contract.
         */

        'permission' => Spatie\Permission\Models\Permission::class,

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your roles. Of course, it
         * is often just the "Role" model but you may use whatever you like.
         *
         * The model you want to use as a Role model needs to implement the
         * `Spatie\Permission\Contracts\Role` contract.
         */

        'role' => Spatie\Permission\Models\Role::class,
    ],

    'table_names' => [

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'roles' => 'roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your permissions. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'permissions' => 'permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_permissions' => 'model_has_permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models roles. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_roles' => 'model_has_roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'role_has_permissions' => 'role_has_permissions',
    ],

    'column_names' => [

        /*
         * Change this if you want to name the related model primary key other than
         * `model_id`.
         *
         * For example, this would be nice if your primary keys are all UUIDs. In
         * that case, name this `model_uuid`.
         */
        'model_morph_key' => 'model_id',
    ],

    'cache' => [

        /*
         * By default all permissions are cached for 24 hours to speed up performance.
         * When permissions or roles are updated the cache is flushed automatically.
         */

        'expiration_time' => \DateInterval::createFromDateString('24 hours'),

        /*
         * The cache key used to store all permissions.
         */

        'key' => 'spatie.permission.cache',

        /*
         * When checking for a permission against a model by passing a Permission
         * instance to the check, this key determines what attribute on the
         * Permissions model is used to cache against.
         *
         * Ideally, this should match your preferred way of checking permissions, eg:
         * `$user->can('view-posts')` would be 'name'.
         */

        'model_key' => 'name',

        /*
         * You may optionally indicate a specific cache driver to use for permission and
         * role caching using any of the `store` drivers listed in the cache.php config
         * file. Using 'default' here means to use the `default` set in cache.php.
         */

        'store' => 'default',
    ],

    /*
     * When set to true, the required permission/role names are added to the exception
     * message. This could be considered an information leak in some contexts, so
     * the default setting is false here for optimum safety.
     */

    'display_permission_in_exception' => false,
];
