
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.moment = require('moment');

import Vue2Filters from 'vue2-filters' 
Vue.use(Vue2Filters)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('infinite-loading', require('vue-infinite-loading').default);
Vue.component('unit-action-history', require('./components/ActionHistory.vue').default);
Vue.component('unit-price-history', require('./components/UnitPriceHistory.vue').default);
Vue.component('user-member-component', require('./components/user/UserMemberComponent.vue').default);
Vue.component('user-activities-component', require('./components/user/UserActivitiesComponent.vue').default);
Vue.component('unit-availability-statistic', require('./components/dashboard/UnitAvailabilityStatisticComponent').default);
Vue.component('unit-activity-statistic', require('./components/dashboard/UnitActivityStatisticComponent').default);
Vue.component('user-summary-card', require('./components/user/UserSummaryItemCardComponent').default);


Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});
Vue.filter('toCurrencyUSD', function(value) {
	let val = (value/1).toFixed(2).replace(',', '.')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
});
const app = new Vue({
    el: '#app'
});
