@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form method="GET" action="{{ route('admin.projects.index') }}" name="user-search-from" novalidate="novalidate" autocomplete="false">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                           {{ __("Project List") }} : <a href="{{route('admin.projects.create')}}" class="btn btn-primary btn-sm ">{{ __("Create New") }} {{ __("Project") }}</a>
                        </div>
                        <div class="col-sm-12 col-md-auto" style="width:400px;">                           
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" placeholder="Name, Short Code ..." aria-label="Name, email or phone number" aria-describedby="button-search" name="term" value="{{ Request::query('term') ? Request::query('term') : '' }}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" id="search-button">Search</button>
                                    <a class="btn btn-outline-secondary" data-toggle="collapse" href="#sub-header-collapse">More Filter</a>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>  
                <div class="card-sub-header card-bg-grey collapse {{ array_except(Request::query(),'page') ? 'show' : '' }}" id="sub-header-collapse">
                    <div class="sub-header-box-wrapper">
                        <div class="form-row">                           
                            <div class="input-group input-group-sm col-md-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="status">Unit Type:</label>
                                </div>                                  
                                <select class="form-control" name="status" id="status">
                                    <option value="all" {{ Request::query('status') == 'all' ? 'selected' : '' }}>All</option>
                                    <option value="active" {{ Request::query('status') == 'active' ? 'selected' : '' }}>Active</option>
                                    <option value="removed" {{ Request::query('status') == 'removed' ? 'selected' : '' }}>Removed</option>
                                </select>
                            </div>                             
                            <button class="btn btn-sm btn-primary" type="submit" id="fileter-button">Show</button>
                            <a href="{{ route('admin.projects.index') }}" class="btn btn-secondary btn-sm ml-md-2">Clear Filter</a>
                        </div>
                    </div>
                </div>             
                <div class="card-body p-0">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <table class="table table-hover table-action mb-0">
                        <thead>
                            <tr>                                
                                <th scope="col">Name</th>
                                <th scope="col" width="50px">{{ __("Published") }}</th>
                                <th scope="col" width="100px">Short Code</th>
                                <th scope="col" width="150px">Unit Type</th>                                
                                <th scope="col" width="150px">Created By</th>
                                <th scope="col" width="170px">Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($projects as $project)
                            <tr class="{{ $project->trashed() ? 'table-secondary' : '' }}">
                                <td scope="row" class="action-td">
                                    <a href="{{ route('admin.projects.edit',['id'=>$project->id]) }}" class="title">{{ $project->name }}</a>
                                    <span class="d-block">{{ $project->name_en }}</span>
                                    @include('admin.project.action')
                                </td>
                                <td>{!! $project->getPublishedHtmlStatus()  !!}</td>
                                <td>{{ $project->short_code }}</td>
                                <td><a href="{{ route('admin.unit_types.index', ['project'=>$project->id]) }}" class="title">{{ $count = $project->unit_types_count }} {{ str_plural('Unit Type',$count) }}</a></td>
                                <td>{{ $project->createdBy->name }}</td>
                                <td>
                                    @if( $project->trashed() )
                                    <ul class="mb-0 text-couple">                                       
                                        <li>
                                            <p>
                                                <small>Created At:</small>
                                                <span class="title">{{ $project->created_at }}</span>
                                            </p>                                            
                                        </li>
                                        <li>
                                            <p>
                                                <small>Deleted At:</small>
                                                <span class="title">{{ $project->deleted_at }}</span>
                                            </p>
                                        </li>
                                    </ul>
                                    @else
                                    <ul class="mb-0 text-couple">                                       
                                        <li>
                                            <p>
                                                <small>Created At:</small>
                                                <span class="title">{{ $project->created_at }}</span>
                                            </p>                                            
                                        </li>
                                        <li>
                                            <p>
                                                <small>Updated At:</small>
                                                <span class="title">{{ $project->updated_at }}</span>
                                            </p>
                                        </li>
                                    </ul>
                                    @endif
                                </td>


                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </form>
            {{ $projects->appends(request()->except(['page','_token'])) }}
        </div>
    </div>
</div>
@endsection
