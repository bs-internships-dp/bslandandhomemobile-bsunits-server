<li class="nav-item">
    <a class="nav-link" href="{{ route('home') }}">Dashboard</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('admin.contracts.index') }}">{{ __('Contracts') }}</a>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __("Report") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('admin.units.report.sale') }}">{{ __("Unit Sale Report") }}</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Users
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('admin.user.all') }}">User List</a>
    </div>
</li>