<li class="nav-item">
    <a class="nav-link" href="{{ route('home') }}">Dashboard</a>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __("Contracts") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{route('admin.contracts.index')}}">{{ __("Contracts") }}</a>
        <a class="dropdown-item" href="{{route('admin.unit_contract_requests.index')}}">{{ __("Unit Contract Requests") }}</a>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('contracts.template') }}">{{ __("Contract Template") }}</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('admin.units.index') }}">{{ __("Units") }}</a>
</li>